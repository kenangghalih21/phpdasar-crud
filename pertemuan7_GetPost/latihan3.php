<?php 
// $_GET

$mahasiswa = [
	[
	"npm" => "201943500208",
	"nama" => "Kenang Ghalih",
	"email" => "kenangghalih21@gmail.com",
	"jurusan" => "Teknik Informatika",
	"gambar" => "reni.jpg"
	],
	[
	"npm" => "201943500200",
	"email" => "myoko12@gmail.com",
	"nama" => "Myoko Ayuki",
	"jurusan" => "Teknik Industri",
	"gambar" => "kufra.jpg"
	]
];

 ?>

 <!DOCTYPE html>
 <html>
 <head>
 	<title>GET</title>
 </head>
 <body>
 
<h1>Daftar Mahasiswa</h1>
<ul>
<?php foreach ( $mahasiswa as $mhs ) : ?>
	<li>
		<a href="latihan4.php?nama=<?= $mhs["nama"]; ?>&npm=<?= $mhs["npm"]; ?>&email=<?= $mhs["email"]; ?>&jurusan=<?= $mhs["jurusan"]; ?>&gambar=<?= $mhs["gambar"]; ?>"><?= $mhs["nama"]; ?></a>
	</li>
<?php endforeach; ?>
</ul>


 </body>
 </html>