<?php 
// koneksi ke database
$conn = mysqli_connect("localhost", "root", "", "phpdasar1");

// ambil data dari tabel mahasiswa / query data mahasiswa
$result = mysqli_query($conn, "SELECT * FROM mahasiswa1");
// ambil data(fetch) mahasiswa dari objek result
// mysqli_fetch_row() -> mengembalikan array numerik(indeks angka)
// mysqli_fetch_assoc() -> mengembalikan array asosiatif(lsng nama field)
// mysqli_fetch_array() -> leboh fleksibel(mengembalikan keduanya, tp data yg disajikan double, datanya berat,, biasa gunakan assoc
// mysqli_fetch_object() -> harus pake panah , $mhs->mail

// while( $mhs = mysqli_fetch_assoc($result) ) {
// 	var_dump($mhs);
// }
// mengecek apakah ada database mahasiswa
// var_dump($result);
// munculin error
// if( !$result) {
// 	echo mysqli_error($conn);
// }
?>



<!DOCTYPE html>
<html>
<head>
	<title>Halaman Admin</title>
</head>
<body>

<h1>Daftar Mahasiswa</h1>

<table border="1" cellpadding="10" cellspacing="0">
	
	<tr>
		<th>No.</th>
		<th>Aksi</th>
		<th>Gambar</th>
		<th>NPM</th>
		<th>Nama</th>
		<th>Email</th>
		<th>Jurusan</th>
	</tr>

	<?php $i = 1; ?>
	<?php while( $row = mysqli_fetch_assoc($result) ) : ?>
	<tr>
		<td><?= $i; ?></td>
		<td>
			<a href="">ubah</a> |
			<a href="">hapus</a>
		</td>
		<td><img src="img/<?= $row["gambar"]; ?>" width="50"></td>
		<td><?= $row["npm"]; ?></td>
		<td><?= $row["nama"]; ?></td>
		<td><?= $row["email"]; ?></td>
		<td><?= $row["jurusan"]; ?></td>
	</tr>
	<?php $i++; ?>
	<?php endwhile; ?>


</table>


</body>
</html>