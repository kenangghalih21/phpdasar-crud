<?php 
session_start();
require 'functions.php';

// cek ccokie
if ( isset($_COOKIE['id']) && isset($_COOKIE['key']) ) {
	$id = $_COOKIE['id'];
	$key = $_COOKIE['key'];

	// ambil usernmae berdsarkan id
	$result = mysqli_query($conn, "SELECT username FROM user WHERE id = $id");
	$row = mysqli_fetch_assoc($result);

	// cek cookie dan usernmae
	if( $key === hash('sha256', $row['username']) ) {
		$_SESSION['login'] = true;
	}

}

if ( isset($_SESSION["login"]) ){
	header("Location: index.php");
	exit;
}




if( isset($_POST["login"]) ) {

	$username = $_POST["username"];
	$password = $_POST["password"];

	$result = mysqli_query($conn, "SELECT * FROM user WHERE username =
		'$username'");
	// cek username
	if( mysqli_num_rows($result) === 1 ) { //utk menghitung ada brp baris yg dikembalikan dari fungsi select, kalo ketmu nilainya 1, kalo ga 0
		// cek password
		$row = mysqli_fetch_assoc($result); //skrng di dalam row udh ada isi data(id,username,pw yg di acak)
		if (password_verify($password, $row["password"]) ){//ngecek sbh string dgn hash-nya(yg dari db)

			// set session
			$_SESSION["login"] = true; //buat session yg akan dicek di tiap2 halaman

			// cek remember me
			if ( isset($_POST["remember"]) ){
				// buat cookie
				setcookie('id', $row['id'], time()+60);
				setcookie('key', hash('sha256', $row['username']), time()+60); //key=username
			}

			header("Location: index.php"); //ngedirect /arahkan user ke hlm index.php
			exit;
		}
	}

	$error = true;
}


 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Halaman Login</title>
</head>
<body>

<h1>Halaman Login</h1>

<?php if(isset($error) ) : ?>
	<p style="color: red; font-style: italic;">username / password salah</p>
<?php endif; ?>

<form action="" method="post">
	
		<ul>
			<li>
				<label for="username">Username :</label>
				<input type="text" name="username" id="username">
			</li>
			<li>
				<label for="password">Password :</label>
				<input type="password" name="password" id="password">
			</li>
			<li>
				<input type="checkbox" name="remember" id="remember">
				<label for="remember">Remember me</label>

			</li>
			
				<button type="submit" name="login">Login</button>	
			
		</ul>
</form>
<p>
	Belum punya akun? <a href="registrasi.php">Buat akun</a>
</p>


</body>
</html>