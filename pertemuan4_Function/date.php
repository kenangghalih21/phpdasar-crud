<?php 
	// Date
	// Untuk menampilkan tanggal dengan format tertentu
	// echo date("l, d-M-Y");

	// time
// UNIX Timestamp / EPOCH Time
// detik yg sudah berlalu sejak 1 januari 1070
// echo time();

// echo date("l",time()+60*60*24*100); 

// mk time
// membuat sendiri detik dari 1970
// mktime(0,0,0,0,0,0)
// jam, menit, detik, bulan, tanggal, tahun
// echo date ("l", mktime(0,0,0,6,21,2001));

// strtotime
echo date("l", strtotime("21 jun 2001"));
?>