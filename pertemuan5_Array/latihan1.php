<?php 
// array -> variable yg dapat menampung banyak nilai
// elemen pd array boleh memiliki tipe data yg berbeda
// definisi array = pasangan antara key dan value
// key-nya dalah index, yang dimulai dari 0



// membuat array
// cara lama
$hari = array("Senin", "Selasa", "Rabu");
// cara baru
$bulan = ["Januari", "Februari", "Maret"];
$arr1 = [123, "tulisan", false];

// Menampilkan array
// var_dump() -> ngecek isi dari sebuah variable dengan keterangan
// print_r()
var_dump($hari);
echo "<br>";
print_r($bulan);
echo "<br>";

// menampilkan 1 elemen pd array
echo $arr1[0];
echo "<br>";
echo $bulan[1];
echo "<br>";

// menambahkan elemen baru pada array
var_dump($hari);
$hari[] = "Kamis";
$hari[] = "Jum'at";
echo "<br>";
var_dump($hari);

 ?>