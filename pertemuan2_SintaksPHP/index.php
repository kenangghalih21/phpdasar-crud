<?php 
// Pertemuan 2 - PHP Dasar
// Sintaks PHP

// Standar Output
// echo, print
// print_r
// var_dump

// Penulisan sintaks PHP
// 1. PHP di dalam HTML
// 2. HTML di dalam PHP

// Variabel dan Tipe data
// Variabel
// tidak boleh diawali dengan angka, tapi boleh mengandung angka
// $nama = "Kenang Ghalih";
// echo "Halo, nama saya $nama";

// operator
// aritmatika
// + - / * %
// $x = 10;
// $y = 20;
// echo $x*$y;

// operator penggabung string / concatenation /concat
// .
// $nama_depan = "Kenang";
// $nama_belakang = "Ghalih";
// echo $nama_depan . " " . $nama_belakang;

// operator assignment
// =, +=, -=, *=, /=, %=, .=
// $x = 1;
// $x -= 5;
// echo $x;
// $nama = "Kenang";
// $nama .= " ";
// $nama .= "Ghalih";
// echo $nama;

// perbandingan
// <, >, <=, >=, ==, !=
// biasa dipake dalam pengkondisian
// var_dump(1 == "1");

// operator identitas
// ===, !==
// var_dump(1 === "1");

// operator logika
// &&, ||, !
// dipake dalam pengkondisian
$x = 30;
var_dump($x < 20 || $x % 2 == 0); //kondisi && bil.genap

?>