<?php 

// hapus session
session_start();
$_SESSION = []; //diisi array kosong, supaya yakin sessionnya hilang
session_unset();
session_destroy();

// hapus cookie
setcookie('id', '', time()-3600); 
setcookie('key', '', time()-3600); 


header("Location: login.php");
exit;

 ?>