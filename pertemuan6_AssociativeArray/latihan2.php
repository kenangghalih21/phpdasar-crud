<?php 
// $mahasiswa = [
// 	["Kenang Ghalih", "201943500208", "kenangghalih21@gmail.com", "Teknik Informatika"],
// 	["Johny Hwang", "201943500098", "jhony@gmail.com", "Teknik Industri"]
// ];

// array associative
// definisinya sama spt array numerik, kecuali
// key-nya adalah string yg kita buat sendri
$mahasiswa = [
	[
	"npm" => "201943500208",
	"nama" => "Kenang Ghalih",
	"email" => "kenangghalih21@gmail.com",
	"jurusan" => "Teknik Informatika",
	"gambar" => "reni.jpg"
	],
	[
	"npm" => "201943500200",
	"email" => "myoko12@gmail.com",
	"nama" => "Myoko Ayuki",
	"jurusan" => "Teknik Industri",
	"gambar" => "kufra.jpg"
	]
];
 ?>
 
 <!DOCTYPE html>
 <html>
 <head>
 	<title>Daftar Mahasiswa</title>
 </head>
 <body>
 
	<h1>Datar Mahasiswa</h1>

	<?php foreach( $mahasiswa as $mhs) : ?>
		<ul>
			<li>
				<img src="img/<?= $mhs["gambar"]; ?>">
			</li>
			<li>Nama : <?= $mhs["nama"]; ?></li>
			<li>NPM : <?= $mhs["npm"]; ?></li>
			<li>Jurusan : <?= $mhs["jurusan"]; ?></li>
			<li>Email : <?= $mhs["email"]; ?></li>
		</ul>
	<?php endforeach; ?>

 </body>
 </html>

 