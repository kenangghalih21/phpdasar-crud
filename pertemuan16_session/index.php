<?php 
session_start();

if ( !isset($_SESSION["login"]) ){ //kalo  gada session login maka kirim ke halaman login
	header("Location: login.php");
}

require 'functions.php'; // menghubungkan index dengan functions
$mahasiswa = query("SELECT * FROM mahasiswa1"); //mengambil fungsi query yg ada di hlm functions utk mendapat seluruh data mahasiswa, lalu masukkan kedalam variabel $mahasiswa

// jika tombol cari di klik, maka akan timpa $mahasiswa sesuai pencarian
if( isset($_POST["cari"]) ) { //algo -> kalo tombol cari dipencet, ambil apapun yg diketik oleh user, masuk ke function cari(functions line 68)
	$mahasiswa = cari($_POST["keyword"]); //function cari mendapat data apapun dari user
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Halaman Admin</title>
</head>
<body>



<h1>Daftar Mahasiswa</h1>

<a href="tambah.php">Tambah data mahasiswa</a>
<br><br>

<form action="" method="post">
	
	<input type="text" name="keyword" size="40" autofocus placeholder="masukkan keyword pencarian.." autocomplete="off">
	<button type="submit" name="cari">Cari</button>
</form>
<br>
<table border="1" cellpadding="10" cellspacing="0">
	
	<tr>
		<th>No.</th>
		<th>Aksi</th>
		<th>Gambar</th>
		<th>NPM</th>
		<th>Nama</th>
		<th>Email</th>
		<th>Jurusan</th>
	</tr>

	<?php $i = 1; ?>
	<?php foreach ($mahasiswa as $row) : ?>
	<tr>
		<td><?= $i; ?></td>
		<td>
			<a href="ubah.php?id=<?= $row["id"]; ?>">ubah</a> |
			<a href="hapus.php?id=<?= $row["id"]; ?>" onclick="return confirm('Apakah anda yakin ingin menghapus data?')">hapus</a>
		</td>
		<td><img src="img/<?= $row["gambar"]; ?>" width="50"></td>
		<td><?= $row["npm"]; ?></td>
		<td><?= $row["nama"]; ?></td>
		<td><?= $row["email"]; ?></td>
		<td><?= $row["jurusan"]; ?></td>
	</tr>
	<?php $i++; ?>
	<?php endforeach; ?>


</table>

<a href="logout.php"; style="color: red;">Logout</a>
</body>
</html>