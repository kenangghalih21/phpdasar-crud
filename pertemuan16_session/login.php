<?php 
session_start();

if ( isset($_SESSION["login"]) ){
	header("Location: index.php");
	exit;
}

require 'functions.php';


if( isset($_POST["login"]) ) {

	$username = $_POST["username"];
	$password = $_POST["password"];

	$result = mysqli_query($conn, "SELECT * FROM user WHERE username =
		'$username'");
	// cek username
	if( mysqli_num_rows($result) === 1 ) { //utk menghitung ada brp baris yg dikembalikan dari fungsi select, kalo ketmu nilainya 1, kalo ga 0
		// cek password
		$row = mysqli_fetch_assoc($result); //skrng di dalam row udh ada isi data(id,username,pw yg di acak)
		if (password_verify($password, $row["password"]) ){//ngecek sbh string dgn hash-nya(yg dari db)

			// set session
			$_SESSION["login"] = true; //buat session yg akan dicek di tiap2 halaman


			header("Location: index.php"); //ngedirect /arahkan user ke hlm index.php
			exit;
		}
	}

	$error = true;
}


 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Halaman Login</title>
</head>
<body>

<h1>Halaman Login</h1>

<?php if(isset($error) ) : ?>
	<p style="color: red; font-style: italic;">username / password salah</p>
<?php endif; ?>

<form action="" method="post">
	
		<ul>
			<li>
				<label for="username">Username :</label>
				<input type="text" name="username" id="username">
			</li>
			<li>
				<label for="password">Password :</label>
				<input type="password" name="password" id="password">
			</li>
			
				<button type="submit" name="login">Login</button>	
			
		</ul>
</form>
<p>
	Belum punya akun? <a href="registrasi.php">Buat akun</a>
</p>


</body>
</html>