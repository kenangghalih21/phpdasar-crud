<?php 
// pengulangan
// for
// while
// do.. while
// foreach: pengulangan khusus array

// for ( $i = 0; $i < 5 ; $i++ ) { 
// 	# code...
// 	echo "Hello World! <br>";
// }

$i = 0;
while ( $i <= 5) {
	# code...
	echo "Hello World! <br>";
$i++; //wajib ada
}

// do while
// $i = 0;
// do {
// 	# code...
// 	echo "Hello World! <br>";
// $i++;
// } while ( $i <= 6);

// do while kalo false dijalankan sekali, kalo while-> gak.

 ?>